## Create deployment
```bash
kubectl create -f boot-docker-deployment.yaml
```

## Create service

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.
```bash
kubectl create -f boot-docker-service.yaml
```

## Get service info
```bash
kubectl get svc
```

## Delete service 
```bash
kubectl delete -f boot-docker-service.yaml
```

## Delete deployment
```bash
kubectl delete -f boot-docker-deployment.yaml
```

## Create Deployment and Service together
```bash
kubectl create -f boot-docker.yaml
```

## Delete Deployment and Service
```bash
kubectl delete -f boot-docker.yaml
```
## Delete by Name
```bash
kubectl delete deployment/boot-docker-deployment svc/boot-docker-service
```
